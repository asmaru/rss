<?php

declare(strict_types=1);
/**
 * MIT License
 *
 * Copyright (c) 2016. Stephan Reifsteck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace asmaru\rss\element;

use DateTime;
use DateTimeInterface;
use DOMDocument;
use DOMElement;

/**
 * Class Channel
 *
 * @package asmaru\rss\element
 */
class Channel extends AbstractElement {

	/**
	 * @var string
	 */
	private string $title = '';

	/**
	 * @var string
	 */
	private string $link = '';

	/**
	 * @var string
	 */
	private string $description = '';

	/**
	 * @var string
	 */
	private string $language;

	/**
	 * @var string
	 */
	private string $copyright;

	/**
	 * @var string
	 */
	private string $managingEditor;

	/**
	 * @var string
	 */
	private string $webMaster;

	/**
	 * @var DateTime
	 */
	private DateTime $pubDate;

	/**
	 * @var DateTime
	 */
	private DateTime $lastBuildDate;

	/**
	 * @var string
	 */
	private string $generator;

	/**
	 * @var string
	 */
	private string $docs;

	/**
	 * @var int
	 */
	private int $ttl;

	/**
	 * @var Item[]
	 */
	private array $items = [];

	/**
	 * @param string $title
	 * @return Channel
	 */
	public function setTitle(string $title): Channel {
		$this->title = $title;
		return $this;
	}

	/**
	 * @param string $link
	 * @return Channel
	 */
	public function setLink(string $link): Channel {
		$this->link = $link;
		return $this;
	}

	/**
	 * @param string $description
	 * @return Channel
	 */
	public function setDescription(string $description): Channel {
		$this->description = $description;
		return $this;
	}

	/**
	 * @param string $language
	 * @return Channel
	 */
	public function setLanguage(string $language): Channel {
		$this->language = $language;
		return $this;
	}

	/**
	 * @param string $copyright
	 * @return Channel
	 */
	public function setCopyright(string $copyright): Channel {
		$this->copyright = $copyright;
		return $this;
	}

	/**
	 * @param string $managingEditor
	 * @return Channel
	 */
	public function setManagingEditor(string $managingEditor): Channel {
		$this->managingEditor = $managingEditor;
		return $this;
	}

	/**
	 * @param string $webMaster
	 * @return Channel
	 */
	public function setWebMaster(string $webMaster): Channel {
		$this->webMaster = $webMaster;
		return $this;
	}

	/**
	 * @param DateTime $pubDate
	 * @return Channel
	 */
	public function setPubDate(DateTime $pubDate): Channel {
		$this->pubDate = $pubDate;
		return $this;
	}

	/**
	 * @param string $generator
	 * @return Channel
	 */
	public function setGenerator(string $generator): Channel {
		$this->generator = $generator;
		return $this;
	}

	/**
	 * @param DateTime $lastBuildDate
	 * @return Channel
	 */
	public function setLastBuildDate(DateTime $lastBuildDate): Channel {
		$this->lastBuildDate = $lastBuildDate;
		return $this;
	}

	/**
	 * @param string $docs
	 * @return Channel
	 */
	public function setDocs(string $docs): Channel {
		$this->docs = $docs;
		return $this;
	}

	/**
	 * @param int $ttl
	 * @return Channel
	 */
	public function setTtl(int $ttl): Channel {
		$this->ttl = $ttl;
		return $this;
	}

	/**
	 * @return Item
	 */
	public function createItem(): Item {
		$item = new Item();
		$this->items[] = $item;
		return $item;
	}

	/**
	 * @param DOMDocument $document
	 * @return DOMElement
	 */
	public function build(DOMDocument $document): DOMElement {
		$channel = $document->createElement('channel');

		$channel->appendChild($this->createTextElement($document, 'title', $this->title));
		$channel->appendChild($this->createTextElement($document, 'link', $this->link));
		$channel->appendChild($this->createTextElement($document, 'description', $this->description));

		if (!empty($this->language)) {
			$channel->appendChild($this->createTextElement($document, 'language', $this->language));
		}

		if (!empty($this->copyright)) {
			$channel->appendChild($this->createTextElement($document, 'copyright', $this->copyright));
		}

		if (!empty($this->managingEditor)) {
			$channel->appendChild($this->createTextElement($document, 'managingEditor', $this->managingEditor));
		}

		if (!empty($this->webMaster)) {
			$channel->appendChild($this->createTextElement($document, 'webMaster', $this->webMaster));
		}

		if (!empty($this->pubDate)) {
			$channel->appendChild($this->createTextElement($document, 'pubDate', $this->pubDate->format(DateTimeInterface::RSS)));
		}

		if (!empty($this->lastBuildDate)) {
			$channel->appendChild($this->createTextElement($document, 'lastBuildDate', $this->lastBuildDate->format(DateTimeInterface::RSS)));
		}

		if (!empty($this->generator)) {
			$channel->appendChild($this->createTextElement($document, 'generator', $this->generator));
		}

		if (!empty($this->docs)) {
			$channel->appendChild($this->createTextElement($document, 'docs', $this->docs));
		}

		if (!empty($this->ttl)) {
			$channel->appendChild($this->createTextElement($document, 'ttl', (string)$this->ttl));
		}

		foreach ($this->items as $item) {
			$channel->appendChild($item->build($document));
		}

		return $channel;
	}
}