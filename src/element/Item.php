<?php

declare(strict_types=1);
/**
 * MIT License
 *
 * Copyright (c) 2016. Stephan Reifsteck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace asmaru\rss\element;

use DateTime;
use DateTimeInterface;
use DOMDocument;
use DOMElement;

/**
 * Class Item
 *
 * @package asmaru\rss\element
 */
class Item extends AbstractElement {

	/**
	 * @var string
	 */
	private string $title = '';

	/**
	 * @var string
	 */
	private string $link = '';

	/**
	 * @var string
	 */
	private string $description = '';

	/**
	 * @var string
	 */
	private string $author;

	/**
	 * @var string
	 */
	private string $comments;

	/**
	 * @var string
	 */
	private string $guid;

	/**
	 * @var DateTime
	 */
	private DateTime $pubDate;

	/**
	 * @param string $title
	 * @return Item
	 */
	public function setTitle(string $title): Item {
		$this->title = $title;
		return $this;
	}

	/**
	 * @param string $link
	 * @return Item
	 */
	public function setLink(string $link): Item {
		$this->link = $link;
		return $this;
	}

	/**
	 * @param string $description
	 * @return Item
	 */
	public function setDescription(string $description): Item {
		$this->description = $description;
		return $this;
	}

	/**
	 * @param string $author
	 * @return Item
	 */
	public function setAuthor(string $author): Item {
		$this->author = $author;
		return $this;
	}

	/**
	 * @param string $comments
	 * @return Item
	 */
	public function setComments(string $comments): Item {
		$this->comments = $comments;
		return $this;
	}

	/**
	 * @param string $guid
	 * @return Item
	 */
	public function setGuid(string $guid): Item {
		$this->guid = $guid;
		return $this;
	}

	/**
	 * @param DateTime $pubDate
	 * @return Item
	 */
	public function setPubDate(DateTime $pubDate): Item {
		$this->pubDate = $pubDate;
		return $this;
	}

	/**
	 * @param DOMDocument $document
	 * @return DOMElement
	 */
	public function build(DOMDocument $document): DOMElement {
		$item = $document->createElement('item');

		$item->appendChild($this->createTextElement($document, 'title', $this->title));
		$item->appendChild($this->createTextElement($document, 'link', $this->link));
		$item->appendChild($this->createTextElement($document, 'description', $this->description));

		if (!empty($this->author)) {
			$item->appendChild($this->createTextElement($document, 'author', $this->author));
		}

		if (!empty($this->comments)) {
			$item->appendChild($this->createTextElement($document, 'comments', $this->comments));
		}

		if (!empty($this->guid)) {
			$item->appendChild($this->createTextElement($document, 'guid', $this->guid));
		}

		if (!empty($this->pubDate)) {
			$item->appendChild($this->createTextElement($document, 'pubDate', $this->pubDate->format(DateTimeInterface::RSS)));
		}

		return $item;
	}
}