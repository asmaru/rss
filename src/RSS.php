<?php

declare(strict_types=1);
/**
 * MIT License
 *
 * Copyright (c) 2016. Stephan Reifsteck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace asmaru\rss;

use asmaru\rss\element\Channel;
use DOMDocument;

/**
 * Class RSS
 *
 * @package asmaru\rss
 */
class RSS {

	/**
	 * @var Channel
	 */
	private Channel $channel;

	/**
	 * RSS constructor.
	 */
	public function __construct() {
		$this->channel = new Channel();
	}

	/**
	 * @return Channel
	 */
	public function channel(): Channel {
		return $this->channel;
	}

	public function out(): void {
		header('Content-Type: application/rss+xml');
		echo $this->__toString();
	}

	/**
	 * Render the rss feed
	 *
	 * @return string
	 */
	public function __toString(): string {
		$document = new DOMDocument('1.0', 'UTF-8');
		$rss = $document->createElement('rss');
		$rss->setAttribute('version', '2.0');
		$document->appendChild($rss);
		$rss->appendChild($this->channel()->build($document));
		return $document->saveXML();
	}
}