<?php
/**
 * MIT License
 *
 * Copyright (c) 2016. Stephan Reifsteck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace asmaru\rss;

use asmaru\rss\element\Channel;
use DateTime;
use DateTimeZone;
use DOMDocument;
use PHPUnit\Framework\TestCase;

class ChannelTest extends TestCase {

	public function testCanSetRequiredElements() {
		$channel = new Channel();
		$channel->setTitle('title&?<>üöä');
		$channel->setLink('http://example.org/?a=b&c=d');
		$channel->setDescription('description&?<>üöä');
		$expected = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<channel><title>title&amp;?&lt;&gt;üöä</title><link>http://example.org/?a=b&amp;c=d</link><description>description&amp;?&lt;&gt;üöä</description></channel>' . "\n";
		$document = new DOMDocument('1.0', 'UTF-8');
		$document->appendChild($channel->build($document));
		static::assertEquals($expected, $document->saveXML());
	}

	public function testCanSetLanguage() {
		$channel = new Channel();
		$channel->setLanguage('de-de');
		$expected = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<channel><title></title><link></link><description></description><language>de-de</language></channel>' . "\n";
		$document = new DOMDocument('1.0', 'UTF-8');
		$document->appendChild($channel->build($document));
		static::assertEquals($expected, $document->saveXML());
	}

	public function testCanSetCopyright() {
		$channel = new Channel();
		$channel->setCopyright('copyright&?<>üöä');
		$expected = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<channel><title></title><link></link><description></description><copyright>copyright&amp;?&lt;&gt;üöä</copyright></channel>' . "\n";
		$document = new DOMDocument('1.0', 'UTF-8');
		$document->appendChild($channel->build($document));
		static::assertEquals($expected, $document->saveXML());
	}

	public function testCanSetManagingEditor() {
		$channel = new Channel();
		$channel->setManagingEditor('copyright&?<>üöä');
		$expected = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<channel><title></title><link></link><description></description><managingEditor>copyright&amp;?&lt;&gt;üöä</managingEditor></channel>' . "\n";
		$document = new DOMDocument('1.0', 'UTF-8');
		$document->appendChild($channel->build($document));
		static::assertEquals($expected, $document->saveXML());
	}

	public function testCanSetWebMaster() {
		$channel = new Channel();
		$channel->setWebMaster('copyright&?<>üöä');
		$expected = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<channel><title></title><link></link><description></description><webMaster>copyright&amp;?&lt;&gt;üöä</webMaster></channel>' . "\n";
		$document = new DOMDocument('1.0', 'UTF-8');
		$document->appendChild($channel->build($document));
		static::assertEquals($expected, $document->saveXML());
	}

	public function testCanSetPubDate() {
		$channel = new Channel();
		$channel->setPubDate(new DateTime('2000-01-01', new DateTimeZone('+0100)')));
		$expected = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<channel><title></title><link></link><description></description><pubDate>Sat, 01 Jan 2000 00:00:00 +0100</pubDate></channel>' . "\n";
		$document = new DOMDocument('1.0', 'UTF-8');
		$document->appendChild($channel->build($document));
		static::assertEquals($expected, $document->saveXML());
	}

	public function testCanSetLastBuildDate() {
		$channel = new Channel();
		$channel->setLastBuildDate(new DateTime('2000-01-01', new DateTimeZone('+0100)')));
		$expected = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<channel><title></title><link></link><description></description><lastBuildDate>Sat, 01 Jan 2000 00:00:00 +0100</lastBuildDate></channel>' . "\n";
		$document = new DOMDocument('1.0', 'UTF-8');
		$document->appendChild($channel->build($document));
		static::assertEquals($expected, $document->saveXML());
	}

	public function testCanSetGenerator() {
		$channel = new Channel();
		$channel->setGenerator('generator&?<>üöä');
		$expected = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<channel><title></title><link></link><description></description><generator>generator&amp;?&lt;&gt;üöä</generator></channel>' . "\n";
		$document = new DOMDocument('1.0', 'UTF-8');
		$document->appendChild($channel->build($document));
		static::assertEquals($expected, $document->saveXML());
	}

	public function testCanSetDocs() {
		$channel = new Channel();
		$channel->setDocs('docs&?<>üöä');
		$expected = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<channel><title></title><link></link><description></description><docs>docs&amp;?&lt;&gt;üöä</docs></channel>' . "\n";
		$document = new DOMDocument('1.0', 'UTF-8');
		$document->appendChild($channel->build($document));
		static::assertEquals($expected, $document->saveXML());
	}

	public function testCanSetTTl() {
		$channel = new Channel();
		$channel->setTtl(1);
		$expected = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<channel><title></title><link></link><description></description><ttl>1</ttl></channel>' . "\n";
		$document = new DOMDocument('1.0', 'UTF-8');
		$document->appendChild($channel->build($document));
		static::assertEquals($expected, $document->saveXML());
	}
}
