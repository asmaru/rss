<?php
/**
 * MIT License
 *
 * Copyright (c) 2016. Stephan Reifsteck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace asmaru\rss;

use asmaru\rss\element\Item;
use DateTime;
use DateTimeZone;
use DOMDocument;
use PHPUnit\Framework\TestCase;

class ItemTest extends TestCase {

	public function testCanSetRequiredElements() {
		$item = new Item();
		$item->setTitle('title&?<>üöä');
		$item->setLink('http://example.org/?a=b&c=d');
		$item->setDescription('description&?<>üöä');
		$expected = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<item><title>title&amp;?&lt;&gt;üöä</title><link>http://example.org/?a=b&amp;c=d</link><description>description&amp;?&lt;&gt;üöä</description></item>' . "\n";
		$document = new DOMDocument('1.0', 'UTF-8');
		$document->appendChild($item->build($document));
		static::assertEquals($expected, $document->saveXML());
	}

	public function testCanSetAuthor() {
		$item = new Item();
		$item->setAuthor('author&?<>üöä');
		$expected = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<item><title></title><link></link><description></description><author>author&amp;?&lt;&gt;üöä</author></item>' . "\n";
		$document = new DOMDocument('1.0', 'UTF-8');
		$document->appendChild($item->build($document));
		static::assertEquals($expected, $document->saveXML());
	}

	public function testCanSetComments() {
		$item = new Item();
		$item->setComments('comments&?<>üöä');
		$expected = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<item><title></title><link></link><description></description><comments>comments&amp;?&lt;&gt;üöä</comments></item>' . "\n";
		$document = new DOMDocument('1.0', 'UTF-8');
		$document->appendChild($item->build($document));
		static::assertEquals($expected, $document->saveXML());
	}

	public function testCanSetGuid() {
		$item = new Item();
		$item->setGuid('guid&?<>üöä');
		$expected = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<item><title></title><link></link><description></description><guid>guid&amp;?&lt;&gt;üöä</guid></item>' . "\n";
		$document = new DOMDocument('1.0', 'UTF-8');
		$document->appendChild($item->build($document));
		static::assertEquals($expected, $document->saveXML());
	}

	public function testCanSetPubDate() {
		$item = new Item();
		$item->setPubDate(new DateTime('2000-01-01', new DateTimeZone('+0100)')));
		$expected = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<item><title></title><link></link><description></description><pubDate>Sat, 01 Jan 2000 00:00:00 +0100</pubDate></item>' . "\n";
		$document = new DOMDocument('1.0', 'UTF-8');
		$document->appendChild($item->build($document));
		static::assertEquals($expected, $document->saveXML());
	}
}
