[![Codacy Badge](https://api.codacy.com/project/badge/Grade/c8fc1acc38724725a0265da2bdbe906b)](https://www.codacy.com/app/asmaru/rss?utm_source=asmaru@bitbucket.org&amp;utm_medium=referral&amp;utm_content=asmaru/rss&amp;utm_campaign=Badge_Grade)

# \asmaru\rss

`\asmaru\rss` is a simple RSS writer. This component is Licensed under MIT license.

## Usage


```php
$rss = new RSS();
$rss->channel()->setTitle('The title');
$rss->channel()->setLink('http://example.org/');
$rss->channel()->setDescription('The description');

$item = $rss->channel()->createItem();
$item->setTitle('The item title');
$item->setLink('http://example.org/item');
$item->setDescription('The item description');

echo $rss;
```

## Installation

You can install directly via [Composer](https://getcomposer.org/):

```bash
$ composer require asmaru/rss
```

## License

MIT license